```
  _   _      _     ____ _                _             ____            _   
 | |_(_) ___| | __/ ___| |__   __ _  ___| | _____ _ __|  _ \ _   _ ___| |_ 
 | __| |/ __| |/ / |   | '_ \ / _` |/ __| |/ / _ \ '__| |_) | | | / __| __|
 | |_| | (__|   <| |___| | | | (_| | (__|   <  __/ |  |  _ <| |_| \__ \ |_ 
  \__|_|\___|_|\_\\____|_| |_|\__,_|\___|_|\_\___|_|  |_| \_\\__,_|___/\__| tickChecker for Rust
```
[![made-with-Rust](https://img.shields.io/badge/made%20with-rust-red)](https://www.rust-lang.org/)
[![Open Source Love svg2](https://badges.frapsoft.com/os/v2/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)

[中文文档](./Readme_CN.md)

## What's TickChecker

* Check target alive state
* Execute the command after failure
* Execute the remote call after failure (with http or https)
* Write all state to local logs (with log file or json)

In short, TickChecker is a simple management tool for server cloud computing in any field to check whether the target service has survived.

Ok,We Support Protocols:

* Tcp ping (for port check)
* Tcp over tls,like tcp ping
* ICMP
* Http or Https
* Websocket(Check port only,include upgrade protocols)

## install

use rust source
```
    git clone https://github.com/koangel/gTickCheckerRust.git
    cd gTickCheckerRust
    cargo build
```

use binary
* download git release packet
* unzip 
* run `./tickChecker`

## config

```
log_path = "./log/tickChecker.log"
timer_debug = true
timer_log = "./log/timer.log"
# log level
log_level = "debug"
roll_count = 30
# mb
file_size = 50
# zero is cpu nums
work_thread = 0

[[action]]
id = 1
name = "httpAction"
# http or ping or tcp or tcp_tls or websocket
atype = "http"
# http : http://www.baidu.com or https://www.baidu.com ,check use get method
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : http://192.168.1.75/ws (only upgrade)
target = "http://www.baidu.com"
# for https or tcp tls ,skip host and cret check.
skip_tls_check = true
# output result data
output_result = true
# check timeout time,millis
timeout = 10000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "http"
# http method get only
fail = "http://www.163.com/abc?a=1"
# recover do action, http or command
recover_type = "http"
recover = "http://www.163.com/abc?a=2"
# wait loop, millis
count = -1
# tick millis
tick = 60000
```

action config
```
[[action]]
id = 1
name = "httpAction"
# http or ping or tcp or tcp_tls or websocket
atype = "http"
# http : http://www.baidu.com or https://www.baidu.com ,check use get method
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : http://192.168.1.75/ws (only upgrade)
target = "http://www.baidu.com"
skip_tls_check = true
output_result = true
timeout = 10000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "http"
# http method get only
fail = "http://www.163.com/abc?a=1"
# recover do action, http or command
recover_type = "http"
recover = "http://www.163.com/abc?a=2"
# wait loop, 毫秒
count = -1
# tick
tick = 60000

[[action]]
id = 2
name = "pingAction"
# http or ping or tcp or tcp_tls or websocket
atype = "ping"
# http : http://www.baidu.com or https://www.baidu.com
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : http://192.168.1.75/ws (only upgrade)
target = "192.168.1.75"
skip_tls_check = true
output_result = true
timeout = 2000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "command"
# http method get only
fail = 'echo "ping failed ip: {target}"'
# recover do action, http or command
recover_type = "command"
recover = 'echo "recover ping ip: {target}"'
# wait loop, 毫秒
count = -1
# tick
tick = 60000

[[action]]
id = 3
name = "tcpAction"
# http or ping or tcp or tcp_tls or websocket
atype = "tcp"
# http : http://www.baidu.com or https://www.baidu.com
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : http://192.168.1.75/ws (only upgrade)
target = "192.168.1.75:80"
skip_tls_check = true
output_result = true
timeout = 3000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "command"
# http method get only
fail = 'echo "tcp failed ip: {target}"'
# recover do action, http or command
recover_type = "command"
recover = 'echo "tcp ping ip: {target}"'
# wait loop, 毫秒
count = -1
# tick
tick = 60000

[[action]]
id = 4
name = "tlsAction"
# http or ping or tcp or tcp_tls or websocket
atype = "tcp_tls"
# http : http://www.baidu.com or https://www.baidu.com
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : http://192.168.1.75/ws (only upgrade)
target = "192.168.1.75:443"
skip_tls_check = true
output_result = true
timeout = 3000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "http"
# http method get only
fail = "http://www.163.com/abc?a=1"
# recover do action, http or command
recover_type = "http"
recover = "http://www.163.com/abc?a=2"
# wait loop, 毫秒
count = -1
# tick
tick = 60000

[[action]]
id = 5
name = "wsAction"
# http or ping or tcp or tcp_tls or websocket
atype = "websocket"
# http : http://www.baidu.com or https://www.baidu.com
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : ws://192.168.1.75/ws (only upgrade)
target = "ws://192.168.1.75/ws"
skip_tls_check = true
output_result = true
timeout = 3000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "http"
# http method get only
fail = "http://www.163.com/abc?a=1"
# recover do action, http or command
recover_type = "http"
recover = "http://www.163.com/abc?a=2"
# wait loop, 毫秒
count = -1
# tick
tick = 60000
```

## logs directory

```
log/tickChecker.log
log/timer.log
```

## other project

[grapeTimerR](https://github.com/koangel/grapeTimerR)
[grapeTimer](https://github.com/koangel/grapeTimer)

## **鸣谢(Thanks)**

Use Jetbrains Ide for project

[![saythanks](https://img.shields.io/badge/say-thanks-ff69b4.svg)](https://saythanks.io/to/kennethreitz)
[![Generic badge](https://img.shields.io/badge/JetBrains-Goland-<COLOR>.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/JetBrains-CLion-<COLOR>.svg)](https://shields.io/)
