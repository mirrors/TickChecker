use std::error::Error;

pub type CResult<T> = Result<T,Box<dyn Error + 'static>>;

