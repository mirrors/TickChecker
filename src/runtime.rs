use tokio::runtime::{Runtime,Builder};
use lazy_static::*;
use num_cpus;

lazy_static! {
    pub static ref RT:Runtime = Builder::new_multi_thread()
        .worker_threads(num_cpus::get()).build().unwrap();
}

pub fn wait_forever() {
    loop {
        std::thread::sleep(std::time::Duration::from_secs(1));
    }
}