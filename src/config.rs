use serde_derive::{Serialize, Deserialize};
use toml::value::Array;

#[derive(Deserialize,Serialize,Clone)]
pub struct ActionConf {
    pub id:i32,
    pub name:String,
    // http or ping or tcp or tcp_tls or websocket
    pub atype:String,
    //http : http://www.baidu.com or https://www.baidu.com
    //ping : 192.168.1.1
    //tcp : 192.168.1.75:80
    //tcp_tls : 192.168.1.75:443
    //websocket : http://192.168.1.75/ws (only upgrade)
    pub target:String,
    pub skip_tls_check:bool,
    pub output_result:bool,
    pub timeout:i32,
    pub fail_count:i32,
    //faild do action, http or command
    pub fail_type:String,
    pub fail:String,
    pub recover_type:String,
    pub recover:String,
    // loop count
    pub count:i32,
    // 周期，毫秒
    pub tick:i64,
}

#[derive(Deserialize,Serialize,Clone)]
pub struct AppConfig {
    pub log_path:String,
    pub log_level:String,
    pub roll_count:i32,
    pub file_size:i32,
    pub timer_debug:bool,
    pub timer_log:String,
    pub work_thread:i32,

    pub action:Vec<ActionConf>,
}