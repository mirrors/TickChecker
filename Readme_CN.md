```
  _   _      _     ____ _                _             ____            _   
 | |_(_) ___| | __/ ___| |__   __ _  ___| | _____ _ __|  _ \ _   _ ___| |_ 
 | __| |/ __| |/ / |   | '_ \ / _` |/ __| |/ / _ \ '__| |_) | | | / __| __|
 | |_| | (__|   <| |___| | | | (_| | (__|   <  __/ |  |  _ <| |_| \__ \ |_ 
  \__|_|\___|_|\_\\____|_| |_|\__,_|\___|_|\_\___|_|  |_| \_\\__,_|___/\__| tickChecker for Rust
```
[![made-with-Rust](https://img.shields.io/badge/made%20with-rust-red)](https://www.rust-lang.org/)
[![Open Source Love svg2](https://badges.frapsoft.com/os/v2/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)

## 什么是TickChecker

* 检测目标的状态是否存活
* 执行任意本机操作
* 执行Http/Https远程调用
* 记录本地日志用于查阅(记录文本或JSON)

简单的说TickChecker就是一个特别简单的管理工具，用于在任何领域的服务器云计算中，查看目标服务是否存活。

我们支持协议有：

* Tcp ping (for port check)
* Tcp over tls,like tcp ping
* ICMP
* Http or Https
* Websocket(连接端口并不发送数据)

## 安装

使用源码安装
```
    git clone https://github.com/koangel/gTickCheckerRust.git
    cd gTickCheckerRust
    cargo build
```

下载二进制
* 下载Github发布包
* 解压缩到任意目录
* 运行 `./tickChecker`

## 配置文件说明

```
log_path = "./log/tickChecker.log"
timer_debug = true
timer_log = "./log/timer.log"
# log level
log_level = "debug"
roll_count = 30
# mb
file_size = 50
# 工作线程数，0使用机器核心数自动
work_thread = 0

[[action]]
id = 1
name = "httpAction"
# http or ping or tcp or tcp_tls or websocket
atype = "http"
# http : http://www.baidu.com or https://www.baidu.com ,check use get method
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : http://192.168.1.75/ws (only upgrade)
target = "http://www.baidu.com"
# 对于HTTPS或TLS连接跳过检测
skip_tls_check = true
# 输出内容
output_result = true
# 超时时间，毫秒
timeout = 10000
# 失败几次执行代码
fail_count = 2
# 失败执行什么类型行为,http 或 command
fail_type = "http"
# 失败执行http连接，仅支持get
fail = "http://www.163.com/abc?a=1"
# 从失败中恢复执行什么操作
recover_type = "http"
recover = "http://www.163.com/abc?a=2"
# 检测次数，-1无限循环, millis
count = -1
# 每次检测周期
tick = 60000
```

action配置例子
```
[[action]]
id = 1
name = "httpAction"
# http or ping or tcp or tcp_tls or websocket
atype = "http"
# http : http://www.baidu.com or https://www.baidu.com ,check use get method
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : http://192.168.1.75/ws (only upgrade)
target = "http://www.baidu.com"
skip_tls_check = true
output_result = true
timeout = 10000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "http"
# http method get only
fail = "http://www.163.com/abc?a=1"
# recover do action, http or command
recover_type = "http"
recover = "http://www.163.com/abc?a=2"
# wait loop, 毫秒
count = -1
# tick
tick = 60000

[[action]]
id = 2
name = "pingAction"
# http or ping or tcp or tcp_tls or websocket
atype = "ping"
# http : http://www.baidu.com or https://www.baidu.com
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : http://192.168.1.75/ws (only upgrade)
target = "192.168.1.75"
skip_tls_check = true
output_result = true
timeout = 2000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "command"
# http method get only
fail = 'echo "ping failed ip: {target}"'
# recover do action, http or command
recover_type = "command"
recover = 'echo "recover ping ip: {target}"'
# wait loop, 毫秒
count = -1
# tick
tick = 60000

[[action]]
id = 3
name = "tcpAction"
# http or ping or tcp or tcp_tls or websocket
atype = "tcp"
# http : http://www.baidu.com or https://www.baidu.com
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : http://192.168.1.75/ws (only upgrade)
target = "192.168.1.75:80"
skip_tls_check = true
output_result = true
timeout = 3000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "command"
# http method get only
fail = 'echo "tcp failed ip: {target}"'
# recover do action, http or command
recover_type = "command"
recover = 'echo "tcp ping ip: {target}"'
# wait loop, 毫秒
count = -1
# tick
tick = 60000

[[action]]
id = 4
name = "tlsAction"
# http or ping or tcp or tcp_tls or websocket
atype = "tcp_tls"
# http : http://www.baidu.com or https://www.baidu.com
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : http://192.168.1.75/ws (only upgrade)
target = "192.168.1.75:443"
skip_tls_check = true
output_result = true
timeout = 3000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "http"
# http method get only
fail = "http://www.163.com/abc?a=1"
# recover do action, http or command
recover_type = "http"
recover = "http://www.163.com/abc?a=2"
# wait loop, 毫秒
count = -1
# tick
tick = 60000

[[action]]
id = 5
name = "wsAction"
# http or ping or tcp or tcp_tls or websocket
atype = "websocket"
# http : http://www.baidu.com or https://www.baidu.com
# ping : 192.168.1.1
# tcp : 192.168.1.75:80
# tcp_tls : 192.168.1.75:443
# websocket : ws://192.168.1.75/ws (only upgrade)
target = "ws://192.168.1.75/ws"
skip_tls_check = true
output_result = true
timeout = 3000
# failed count do action
fail_count = 2
# failed do action, http or command
fail_type = "http"
# http method get only
fail = "http://www.163.com/abc?a=1"
# recover do action, http or command
recover_type = "http"
recover = "http://www.163.com/abc?a=2"
# wait loop, 毫秒
count = -1
# tick
tick = 60000
```

## logs目录以及各式

```
log/tickChecker.log
log/timer.log
```

## 其他项目

[grapeTimerR](https://github.com/koangel/grapeTimerR)
[grapeTimer](https://github.com/koangel/grapeTimer)

## **鸣谢(Thanks)**

Use Jetbrains Ide for project

[![saythanks](https://img.shields.io/badge/say-thanks-ff69b4.svg)](https://saythanks.io/to/kennethreitz)
[![Generic badge](https://img.shields.io/badge/JetBrains-Goland-<COLOR>.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/JetBrains-CLion-<COLOR>.svg)](https://shields.io/)

